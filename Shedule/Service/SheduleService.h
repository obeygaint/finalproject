//
//  SheduleService.h
//  CoreTest
//
//  Created by Ivan Perelekhov on 14.12.16.
//  Copyright © 2016 Ivan Perelekhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MagicalRecord/MagicalRecord.h>


@interface SheduleService : NSObject

-(void) getNewDataForKey:(NSString*) key;
-(instancetype)init;
@end
