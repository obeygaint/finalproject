//
//  SheduleService.m
//  CoreTest
//
//  Created by Ivan Perelekhov on 14.12.16.
//  Copyright © 2016 Ivan Perelekhov. All rights reserved.
//

#import "SheduleService.h"
#import "DataDecoder.h"
@implementation SheduleService

#pragma Public methods

-(instancetype)init{
    self=[super init];
    if(self){
        
    }
    return self;
}

-(void) getNewDataForKey:(NSString *)key{
    NSString *SERVERIP=@"http://127.0.0.1:5000/";
    NSString *requestURL=[NSString stringWithFormat:@"%@%@",SERVERIP,key];
    //NSString *requestURL=[NSString stringWithFormat:@"http://localhost:5000/%@/",key];
    NSURL *url=[NSURL URLWithString:[requestURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10000];
    [request setHTTPMethod:@"GET"];
    NSURLSessionConfiguration *config=[NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session=[NSURLSession sessionWithConfiguration:config];
    NSURLSessionDataTask *dataTask=[session dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response, NSError *error){
        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            
            NSInteger statusCode = [(NSHTTPURLResponse *)response statusCode];
            
            if (statusCode != 200) {
                NSLog(@"dataTaskWithRequest HTTP status code: %d", statusCode);
                return;
            }
            else{
                NSDictionary *dataJson=[NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                [DataDecoder decodeDataAndSave:dataJson ForKey:key];
            }
        }
    }];
    [dataTask resume];    
}

@end
