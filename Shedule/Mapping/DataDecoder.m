//
//  DataDecoder.m
//  CoreTest
//
//  Created by Ivan Perelekhov on 16.12.16.
//  Copyright © 2016 Ivan Perelekhov. All rights reserved.
//
#import "DataDecoder.h"
#import "Group+CoreDataProperties.h"
#import "Lesson+CoreDataClass.h"
#import "SubLesson+CoreDataClass.h"

@implementation DataDecoder

-(instancetype)init{
    self=[super init];
    if(self){
    }
    return self;
}

-(instancetype)initWithDict:(NSDictionary *)Json{
    self=[super init];
    if(self){
        _dataDict=Json;
    }
    return self;
}

/*

 TODO:
 add validation data;
 add exams saving;
 add exams validation;
*/
+(void)decodeDataAndSave:(NSDictionary *)Json ForKey:(NSString *)Key{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext){
    Group *group = [Group MR_createEntityInContext:localContext];
    group.groupName=Key;
    NSArray *upper=[Json valueForKey:@"upper"][0];
    for (int i=0;i<[upper count];++i){
        NSDictionary *lesson=upper[i];
        if(![[lesson valueForKey:@"hash"]  isEqual: @"0"]){
            Lesson *ENLesson=[Lesson MR_createEntityInContext:localContext];
            ENLesson.lessonHash=[lesson valueForKey:@"hash"];
            ENLesson.dayNum=i/7;
            ENLesson.lessonNum=i%7;
            ENLesson.note=@"";
            ENLesson.show=YES;
            ENLesson.weekType=1;
            //NSArray *subLesson=[lesson valueForKey:@"lesson"];
            for(int j=0;j<[[lesson valueForKey:@"lesson"] count];++j){
                SubLesson *subLesson = [SubLesson MR_createEntityInContext:localContext];
                subLesson.enable=YES;
                subLesson.end=[[lesson valueForKey:@"e_time"][j] intValue];
                subLesson.lessonName=[lesson valueForKey:@"lesson"][j];
                subLesson.lessonType=[lesson valueForKey:@"type_lesson"][j];
                subLesson.room=[lesson valueForKey:@"room"][j];
                subLesson.start=[[lesson valueForKey:@"s_time"][j] intValue];
                subLesson.subGroup=[lesson valueForKey:@"subgroup"][j];
                subLesson.teacherName=[lesson valueForKey:@"name_teacher"][j];
                [ENLesson addSubLessonObject:subLesson];
            }
            [group addLessonsObject:ENLesson];
        }
        
        
    }
        
    NSArray *lower=[Json valueForKey:@"lower"][1];
    for (int i=0;i<[lower count];++i){
        NSDictionary *lesson=lower[i];
        if(![[lesson valueForKey:@"hash"]  isEqual: @"0"]){
            Lesson *ENLesson=[Lesson MR_createEntityInContext:localContext];
            ENLesson.lessonHash=[lesson valueForKey:@"hash"];
            ENLesson.dayNum=i/7;
            ENLesson.lessonNum=i%7;
            ENLesson.note=@"";
            ENLesson.show=YES;
            ENLesson.weekType=0;
            //NSArray *subLesson=[lesson valueForKey:@"lesson"];
            for(int j=0;j<[[lesson valueForKey:@"lesson"] count];++j){
                SubLesson *subLesson = [SubLesson MR_createEntityInContext:localContext];
                subLesson.enable=YES;
                subLesson.end=[[lesson valueForKey:@"e_time"][j] intValue];
                subLesson.lessonName=[lesson valueForKey:@"lesson"][j];
                subLesson.lessonType=[lesson valueForKey:@"type_lesson"][j];
                subLesson.room=[lesson valueForKey:@"room"][j];
                subLesson.start=[[lesson valueForKey:@"s_time"][j] intValue];
                subLesson.subGroup=[lesson valueForKey:@"subgroup"][j];
                subLesson.teacherName=[lesson valueForKey:@"name_teacher"][j];
                [ENLesson addSubLessonObject:subLesson];
                }
            [group addLessonsObject:ENLesson];
            }
        }
    }];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateData" object:nil];
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setObject:Key forKey:@"group"];
}

@end
