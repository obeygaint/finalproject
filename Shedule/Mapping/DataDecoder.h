//
//  DataDecoder.h
//  CoreTest
//
//  Created by Ivan Perelekhov on 16.12.16.
//  Copyright © 2016 Ivan Perelekhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MagicalRecord/MagicalRecord.h>

@interface DataDecoder : NSObject

@property (nonatomic,strong) NSDictionary * dataDict;

-(instancetype)initWithDict:(NSDictionary *)Json;
+(void)decodeDataAndSave:(NSDictionary *)Json ForKey:(NSString *)Key;
@end
