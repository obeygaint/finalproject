//
//  AppDelegate.h
//  Shedule
//
//  Created by Xcode on 12/12/2016.
//  Copyright © 2016 Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

