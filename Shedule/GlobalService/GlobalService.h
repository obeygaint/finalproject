//
//  GlobalService.h
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SheduleService.h"
#import "FetchDataModel.h"

@interface GlobalService : NSObject

@property (nonatomic,strong) SheduleService* sheduleService;
@property (nonatomic,strong) FetchDataModel* fetchModel;

+(id)sharedService;

@end
