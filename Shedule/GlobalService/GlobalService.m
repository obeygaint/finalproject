//
//  GlobalService.m
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import "GlobalService.h"

@implementation GlobalService

@synthesize sheduleService;
@synthesize fetchModel;

+(id)sharedService{
    static GlobalService* sharedService=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedService = [[self alloc]init];
    });
    return sharedService;
}

-(instancetype)init{
    self=[super init];
    if(self){
        self.sheduleService=[[SheduleService alloc]init];
        self.fetchModel=[[FetchDataModel alloc]init];
    }
}

@end
