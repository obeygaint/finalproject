//
//  FetchDataModel.m
//  CoreTest
//
//  Created by Ivan Perelekhov on 20.12.16.
//  Copyright © 2016 Ivan Perelekhov. All rights reserved.
//

#import "FetchDataModel.h"

@implementation FetchDataModel

-(instancetype)init{
    self=[super init];
    if(self){
        
    }
    return self;
}

+(Lesson*)getLessonForDay:(NSInteger)DayNum andPairNum:(NSInteger)pairNum andWeekType:(NSInteger)weekType withKey:(NSString *)key{
    Group *gr = [Group MR_findFirstByAttribute:@"groupName"
                                     withValue:key];
    NSPredicate *weekPredicate=[NSPredicate predicateWithFormat:@"dayNum==%i AND lessonNum==%i AND weekType==%i",DayNum,pairNum,weekType];
    Lesson *arr=[[[gr lessons] filteredSetUsingPredicate:weekPredicate] anyObject];
    return arr;
}

-(NSArray*)getExamsforKey:(NSString *)key{
    return nil;
}

+(NSArray*)getLessonsForWeek:(NSInteger)week withKey:(NSString *)key{
    Group *gr = [Group MR_findFirstByAttribute:@"groupName"
                                     withValue:key];
    NSPredicate *weekPredicate=[NSPredicate predicateWithFormat:@"weekType==%i",week];
    NSArray *arr=[[[gr lessons] filteredSetUsingPredicate:weekPredicate] allObjects];
    return arr;
}

+(NSArray*)getLessonsForDay:(NSInteger)DayNum andWeekType:(NSInteger)week withKey:(NSString *)key{
    Group *gr = [Group MR_findFirstByAttribute:@"groupName"
                                     withValue:key];
    NSPredicate *weekPredicate=[NSPredicate predicateWithFormat:@"weekType==%i AND dayNum==%i",week,DayNum];
    NSArray *arr=[[[gr lessons] filteredSetUsingPredicate:weekPredicate] allObjects];
    return arr;
}

-(void)generateDataForDayWithKey:(NSString *)key{
    
}

+(NSInteger)calculateCurrentLessonNumber{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *str=[dateFormatter stringFromDate:[NSDate date]];
    NSDate *curtime=[dateFormatter dateFromString:str];
    NSCalendar *cal=[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *firstComp=[cal components:
                                 (NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitHour |NSCalendarUnitMinute)
                                       fromDate:[NSDate date]];
    firstComp.month=1;
    firstComp.year=2000;
    firstComp.day=1;
    NSDate *curdate=[cal dateFromComponents:firstComp];
    NSDate *one=[cal dateFromComponents:firstComp];
    
    [firstComp setHour:9];
    [firstComp setMinute:50];
    NSDate *two=[cal dateFromComponents:firstComp];
    
    [firstComp setHour:11];
    [firstComp setMinute:55];
    NSDate *three=[cal dateFromComponents:firstComp];
    
    [firstComp setHour:13];
    [firstComp setMinute:45];
    NSDate *four=[cal dateFromComponents:firstComp];
    
    [firstComp setHour:15];
    [firstComp setMinute:50];
    NSDate *five=[cal dateFromComponents:firstComp];
    
    [firstComp setHour:17];
    [firstComp setMinute:40];
    NSDate *six=[cal dateFromComponents:firstComp];
    
    [firstComp setHour:19];
    [firstComp setMinute:30];
    NSDate *seven=[cal dateFromComponents:firstComp];
    
    [firstComp setHour:23];
    [firstComp setMinute:59];
    
    //NSLog(@"%li ",[self date:test isBetweenDate:three andDate:four]);
    if([curdate compare:one] == NSOrderedAscending){
        return 0;
    }
    else if ([self date:curdate isBetweenDate:one andDate:two]){
        return 1;
    }
    else if ([self date:curdate isBetweenDate:two andDate:three]){
        return 2;
    }
    else if ([self date:curdate isBetweenDate:three andDate:four]){
        return 3;
    }
    else if ([self date:curdate isBetweenDate:four andDate:five]){
        return 4;
    }
    else if ([self date:curdate isBetweenDate:five andDate:six]){
        return 5;
    }
    else if ([self date:curdate isBetweenDate:six andDate:seven]){
        return 6;
    }
    else if ([curdate compare:seven] == NSOrderedDescending){
        return 7;
    }
    return 8;
}

-(BOOL)calculateCurrentWeekNumber{
    return false;
}

#pragma Time methods
+(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES; 
}

@end
