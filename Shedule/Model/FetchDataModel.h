//
//  FetchDataModel.h
//  CoreTest
//
//  Created by Ivan Perelekhov on 20.12.16.
//  Copyright © 2016 Ivan Perelekhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lesson+CoreDataClass.h"
#import "Exam+CoreDataClass.h"
#import "Group+CoreDataClass.h"
#import <MagicalRecord/MagicalRecord.h>

@interface FetchDataModel : NSObject

@property (nonatomic,strong) NSArray *availableLessons;
@property (nonatomic,strong) NSArray *unavailableLessons;
-(instancetype)init;
+(NSArray*)getLessonsForWeek:(NSInteger)week withKey:(NSString *)key;
+(Lesson*)getLessonForDay:(NSInteger)DayNum andPairNum:(NSInteger)pairNum andWeekType:(NSInteger)weekType withKey:(NSString *)key;
+(NSArray*)getLessonsForDay:(NSInteger)DayNum andWeekType:(NSInteger)week withKey:(NSString *)key;
+(NSArray*)getExamsforKey:(NSString*)key;
-(void)generateDataForDayWithKey:(NSString*)key;
+(NSInteger)calculateCurrentLessonNumber;
@end
