//
//  lessonCellTableViewCell.h
//  Shedule
//
//  Created by Xcode on 20/12/2016.
//  Copyright © 2016 Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface lessonCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLessonLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLessonLabel;

@end
