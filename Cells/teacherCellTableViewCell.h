//
//  teacherCellTableViewCell.h
//  Shedule
//
//  Created by Xcode on 22/12/2016.
//  Copyright © 2016 Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface teacherCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeAndNameLectionLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameAndRoomLabel;

@end
