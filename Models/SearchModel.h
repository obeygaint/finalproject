//
//  SearchModel.h
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SheduleService.h"
#import "Group+CoreDataClass.h"
#import "Lesson+CoreDataClass.h"
#import "SubLesson+CoreDataClass.h"
#import <MagicalRecord/MagicalRecord.h>

@interface SearchModel : NSObject
@property(nonatomic,strong) SheduleService* service;
@property(nonatomic,strong) NSArray<Group*> *tableData;
-(instancetype)init;
-(void)generateNewData;
-(void)loadNewDataFromServerForKey:(NSString*) key;
@end
