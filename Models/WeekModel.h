//
//  WeekModel.h
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FetchDataModel.h"
#import <MagicalRecord/MagicalRecord.h>
@interface WeekModel : NSObject
@property (nonatomic,strong) NSUserDefaults* userDef;
@property (nonatomic,strong) NSArray* Lessons;//Пары
@property (nonatomic,strong) NSMutableDictionary* sectionsInfo;
-(instancetype)init;
-(void)fetchLessonsAndPrepareForWeek:(NSNumber*)Week;
@end
