//
//  WeekModel.m
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import "WeekModel.h"

@implementation WeekModel
-(instancetype)init{
    self=[super init];
    if(self){
        self.userDef=[NSUserDefaults standardUserDefaults];
        self.sectionsInfo=[[NSMutableDictionary alloc] init];
        //self.Lessons=[[NSArray alloc]init];
    }
    return self;
}

-(void)fetchLessonsAndPrepareForWeek:(NSNumber*)Week{
    NSString *group=[self.userDef valueForKey:@"group"];
    NSInteger weekType=[Week integerValue]%2;
    NSArray *fetchResult=[FetchDataModel getLessonsForWeek:weekType withKey:group];
    self.Lessons=fetchResult;
    for(int i=0;i<7;++i){
        NSPredicate *filter=[NSPredicate predicateWithFormat:@"dayNum==%d",i];
        NSArray* lessons=[_Lessons filteredArrayUsingPredicate:filter];
        NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"lessonNum"
                                                                    ascending: YES];
        lessons=[lessons sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
        if([lessons count])
            [self.sectionsInfo setObject:lessons forKey:[NSString stringWithFormat:@"%d",i]];
    }
    
    //[self calculateSections];
}

-(void)calculateSections{}
//    NSMutableArray *arr=[[NSMutableArray alloc] initWithObjects:@0,@0,@0,@0,@0,@0,@0,nil];
//    for (Lesson* les in _Lessons) {
//        NSUInteger index=[les dayNum];
//        NSNumber *newValue=[[NSNumber alloc] initWithInteger:([[arr objectAtIndex:index] integerValue]+1)];
//        [arr setObject:newValue atIndexedSubscript:index];
//    }
//}

@end
