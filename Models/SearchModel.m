//
//  SearchModel.m
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import "SearchModel.h"

@implementation SearchModel

-(instancetype)init{
    self=[super init];
    if(self){
        self.service=[[SheduleService alloc]init];
    }
    return self;
}

-(void)generateNewData{
    self.tableData=[Group MR_findAll];
}

-(void)loadNewDataFromServerForKey:(NSString *)key{
    [self.service getNewDataForKey:key];
}

@end
