//
//  DayModel.h
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FetchDataModel.h"
#import <MagicalRecord/MagicalRecord.h>

@interface DayModel : NSObject
@property (nonatomic,strong) NSUserDefaults* userDef;
@property (nonatomic,strong) NSArray* aLessons;//Прошедшие пары
@property (nonatomic,strong) NSArray* unLessons;//Пары ,следующие
-(instancetype)init;
-(void)fetchLessonsAndPrepare;
@end
