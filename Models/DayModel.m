//
//  DayModel.m
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import "DayModel.h"


@implementation DayModel
-(instancetype)init{
    self=[super init];
    if(self){
        self.userDef=[NSUserDefaults standardUserDefaults];
        self.aLessons=[[NSArray alloc]init];
        self.unLessons=[[NSArray alloc]init];
    }
    return self;
}

-(void)fetchLessonsAndPrepare{
    NSNumber *curLessonNum=[NSNumber numberWithInteger:[FetchDataModel calculateCurrentLessonNumber]];
    NSDate *today=[NSDate date];
    NSDateFormatter *format=[[NSDateFormatter alloc]init];
    [format setDateFormat:@"c"];
    NSInteger dayNum=[[format stringFromDate:today] integerValue]-2;
    if(dayNum==-1){
        dayNum=6;
    }
    NSUserDefaults* userDef=[NSUserDefaults standardUserDefaults];
    NSString *group=[userDef valueForKey:@"group"];
    NSInteger week=((int)[userDef valueForKey:@"week"])%2;     //NSInteger week=0;
    NSArray *fetcherLessons=[FetchDataModel getLessonsForDay:dayNum andWeekType:week withKey:group];
    NSPredicate *LeftPredicate=[NSPredicate predicateWithFormat:@"lessonNum < %@",curLessonNum];
    NSPredicate *RightPredicate=[NSPredicate predicateWithFormat:@"lessonNum >= %@",curLessonNum];
    self.aLessons=[fetcherLessons filteredArrayUsingPredicate:RightPredicate];
    self.unLessons=[fetcherLessons filteredArrayUsingPredicate:LeftPredicate];
}
@end

