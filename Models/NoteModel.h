//
//  NoteModel.h
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Group+CoreDataClass.h"
#import "SubLesson+CoreDataClass.h"
#import "Lesson+CoreDataClass.h"
#import <MagicalRecord/MagicalRecord.h>

@interface NoteModel : NSObject
-(instancetype)init;
-(void)saveNote:(NSString *)note forLessonHash:(NSString *)hash;
-(NSString*)loadNoteForLessonHash:(NSString *)hash;
@end
