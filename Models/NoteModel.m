//
//  NoteModel.m
//  Shedule
//
//  Created by Ivan Perelekhov on 23.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import "NoteModel.h"

@implementation NoteModel

-(instancetype)init{
    self=[super init];
    if(self){
        
    }
    return self;
}

-(void)saveNote:(NSString *)note forLessonHash:(NSString *)hash{
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    NSString *groupName=[def valueForKey:@"group"];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        Group *group=[Group MR_findFirstByAttribute:@"groupName" withValue:groupName inContext:localContext];
        NSPredicate *filter=[NSPredicate predicateWithFormat:@"lessonHash==%@",hash];
        NSArray *lessons=[[[group lessons] allObjects] filteredArrayUsingPredicate:filter];
        for (Lesson *less in lessons) {
            less.note=note;
        }
    }];
}

-(NSString *)loadNoteForLessonHash:(NSString *)hash{
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    NSString *groupName=[def valueForKey:@"group"];
    Group *group=[Group MR_findFirstByAttribute:@"groupName" withValue:groupName];
    NSPredicate *filter=[NSPredicate predicateWithFormat:@"lessonHash==%@",hash];
    NSArray *lessons=[[[group lessons] allObjects] filteredArrayUsingPredicate:filter] ;
    for (Lesson *less in lessons) {
        return less.note;
    }
    return nil;
}
@end
