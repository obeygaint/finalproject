//
//  SearchViewController.m
//  Shedule
//
//  Created by Ivan Perelekhov on 22.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (IBAction)SearchButton:(id)sender{
    NSString *request=[self.searchField text];
    NSString *expressionTeacher = @"(КТ|УЭ|ЭП|РТ)+(бо|бз|со|сз|мо|мз|ао)+[1-6]+[-]+(1+[1-5]|[1-9])";
    NSString *expressionGroup = @"([А-Я]+[а-я]*)+[ ]+[А-Я]+[.]+[А-Я]+[.]";
    NSError *error = NULL;
    
    NSRegularExpression *regexTeacher = [NSRegularExpression regularExpressionWithPattern:expressionTeacher options:NSRegularExpressionCaseInsensitive error:&error];
    NSRegularExpression *regexGroup = [NSRegularExpression regularExpressionWithPattern:expressionGroup options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSTextCheckingResult *Teacher = [regexTeacher firstMatchInString:request options:0 range:NSMakeRange(0, [request length])];
    NSTextCheckingResult *Group = [regexGroup firstMatchInString:request options:0 range:NSMakeRange(0, [request length])];
    if(Teacher || Group){
        [self.model loadNewDataFromServerForKey:request];
        //NSUserDefaults* userDef=[NSUserDefaults standardUserDefaults];
        //[userDef setValue:request forKey:@"group"];
        //[userDef synchronize];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка!"
                                                        message:@"Неверный запрос! \n Пример для групп: КТбо1-1 \n Пример для преподавателя: Иванов И.И."
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Yes", nil];
        [alert show];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.model.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lessonCell"];
    
    if (cell == nil)
    {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"lessonCell"];
    }
    cell.textLabel.text=[[self.model.tableData objectAtIndex:indexPath.row] groupName];
    //[[Group *gr] Lesson]
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setObject:[[self.model.tableData objectAtIndex:indexPath.row] groupName] forKey:@"group"];
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateData" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchField.delegate=self;
    self.Table.delegate=self;
    self.Table.dataSource=self;
    self.model=[[SearchModel alloc]init];
    [self.model generateNewData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y -100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.3];
    [UIView setAnimationBeginsFromCurrentState:TRUE];
    self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y +100., self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView commitAnimations];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
