//
//  WeekViewController.h
//  Shedule
//
//  Created by Xcode on 20/12/2016.
//  Copyright © 2016 Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeekModel.h"
#import "SubLesson+CoreDataClass.h"

@interface WeekViewController : UIViewController <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *lessonsTable;
@property (weak, nonatomic) IBOutlet UILabel *numberWeekLabel;
@property (weak, nonatomic) IBOutlet UILabel *semestrTypeLabel;
@property (nonatomic,strong) WeekModel *model;
@property (strong, nonatomic) NSArray * dayWeek;
@property (strong, nonatomic) NSArray * timeLesson ;
@property (strong,nonatomic) NSString* selectedLessonHash;
@property (strong,nonatomic) NSUserDefaults *userDef;
@property (strong,nonatomic) NSNumber* selectedWeekNumber;
@property (nonatomic,strong) NSIndexPath* indexPath;
@property (nonatomic,strong) NSArray* lessons;


@end
