//
//  WeekViewController.m
//  Shedule
//
//  Created by Xcode on 20/12/2016.
//  Copyright © 2016 Team. All rights reserved.
//

#import "WeekViewController.h"
#import "lessonCellTableViewCell.h"
#import "EditLessonViewController.h"

@interface WeekViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation WeekViewController

-(void)viewWillAppear:(BOOL)animated{
    //[[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (IBAction)Increase:(UIButton *)sender {
    long currentNumberWeek = [self.numberWeekLabel.text integerValue];
    if (currentNumberWeek < 24){
        currentNumberWeek++;
        self.selectedWeekNumber=[NSNumber numberWithLong:currentNumberWeek];
    }
    self.numberWeekLabel.text = [NSString stringWithFormat:@"%ld" ,(long) currentNumberWeek];
    [_model fetchLessonsAndPrepareForWeek:self.selectedWeekNumber];
    [_lessonsTable reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadDataFromNotification)
                                                 name:@"updateData"
                                               object:nil];
    self.dayWeek =[[NSArray alloc]initWithObjects:@"Понедельник",@"Вторник",@"Среда",@"Четверг",@"Пятница",@"Суббота", @"Воскрсенье", nil ];
    self.timeLesson =[[NSArray alloc]initWithObjects:@"1 |8:00 - 9:35",@"2 |9:50 - 11:25",@"3 |11:55 - 13:30",@"4 |13:45 - 15:20",@"5 |15:50 - 17:25",@"6 |17:40 - 19:15", @"7 |19:30 - 21:05", nil ];
    self.model=[[WeekModel alloc]init];
    self.userDef=[NSUserDefaults standardUserDefaults];
    self.selectedWeekNumber=[NSNumber numberWithInteger: [[self.userDef valueForKey:@"week"] integerValue]];
    [self.model fetchLessonsAndPrepareForWeek:self.selectedWeekNumber];
    self.lessons = [self.model Lessons];
    self.lessonsTable.dataSource = self;
    self.lessonsTable.delegate = self;
    
    // Do any additional setup after loading the view.
}

- (IBAction)ReduceWeek:(UIButton *)sender {
    long currentNumberWeek = [self.numberWeekLabel.text integerValue];
    if (currentNumberWeek > 1){
        currentNumberWeek--;
        self.selectedWeekNumber=[NSNumber numberWithLong:currentNumberWeek];
      }
    self.numberWeekLabel.text = [NSString stringWithFormat:@"%ld" ,(long) currentNumberWeek];
    [_model fetchLessonsAndPrepareForWeek:self.selectedWeekNumber];
    [_lessonsTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger count=[[self.model sectionsInfo] count];
    return count;}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSArray *week= [[NSArray alloc]initWithArray:self.dayWeek];
    NSString *string = [week objectAtIndex:section];
    return string;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count=[[[self.model sectionsInfo] valueForKey:[NSString stringWithFormat:@"%ld",(long)section]] count];
    return count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    lessonCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lessonCell"];
    
    if (cell == nil)
    {
        
        cell = [[lessonCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"lessonCell"];
    }
    NSMutableString *lesson = [[NSMutableString alloc]init];
    
    NSArray<Lesson *> *subLesson =[[self.model sectionsInfo] valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    Lesson *less=[subLesson objectAtIndex:indexPath.row] ;
    /*ConstStringALLSubgroups = "ALL"
    ConstStringALLTime = "ALLTIME"
    ConstStringLection = "LECTION"
    ConstStringPractiсe = "PRACTIСE"
     */
    NSDictionary *lessontypes=@{@"LECTION":@"Лек. ",@"PRACTIСE":@"Практ. ",@"LAB":@"Лаб. "};
    for(int i = 0 ; i< [[[less subLesson] allObjects] count]; ++i){
    
    [lesson appendString:[[[[less subLesson] allObjects] objectAtIndex:i] teacherName]];
    [lesson appendString:[[[[less subLesson] allObjects] objectAtIndex:i] lessonName]];
    NSString *lessonType=[[[[less subLesson] allObjects] objectAtIndex:i]  lessonType];
    //NSLog(@"%@",lessonType);
    [lesson appendString:[lessontypes objectForKey:lessonType]];
    [lesson appendString:[[[[less subLesson] allObjects] objectAtIndex:i] room]];

    if ([[[[less subLesson] allObjects] objectAtIndex:i] end ] != 30){
        [lesson appendString:@" ("];
        [lesson appendString: [NSString stringWithFormat:@"%i",[[[[less subLesson] allObjects] objectAtIndex:i] start]]];
        [lesson appendString:@" - "];
        [lesson appendString: [NSString stringWithFormat:@"%i",[[[[less subLesson] allObjects] objectAtIndex:i] end]]];
        [lesson appendString:@") "];
    }
    if (![[[[[less subLesson] allObjects] objectAtIndex:i]subGroup] isEqualToString:@"ALL"]){
        [lesson appendString:[[[[less subLesson] allObjects] objectAtIndex:i] subGroup]];
    }
    [lesson appendString:@"\n"];
    }
    cell.nameLessonLabel.text = lesson;
    cell.timeLessonLabel.text = [self.timeLesson objectAtIndex:[less lessonNum]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray<Lesson *> *subLesson =[[self.model sectionsInfo] valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.section]];
    Lesson *less=[subLesson objectAtIndex:indexPath.row];
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    [def setValue:less.lessonHash forKey:@"selHash"];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)reloadDataFromNotification{
    [_model fetchLessonsAndPrepareForWeek:self.selectedWeekNumber];
    [_lessonsTable reloadData];
}

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if([[segue destinationViewController] isKindOfClass:[EditLessonViewController class]]){
//        NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
//        //[def setValue:self.selectedLessonHash forKey:]
//        EditLessonViewController *dest=[segue destinationViewController];
//        dest.lessonHash=self.selectedLessonHash;
//        //dest.data=self;
//       }
//}
//       
@end
