//
//  SearchViewController.h
//  Shedule
//
//  Created by Ivan Perelekhov on 22.12.16.
//  Copyright © 2016 Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchModel.h"

@interface SearchViewController : UIViewController <UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITextField *searchField;
@property (nonatomic,strong) SearchModel *model;
@property (strong, nonatomic) IBOutlet UITableView *Table;
- (IBAction)SearchButton:(id)sender;

@end
