//
//  EditLessonViewController.m
//  Shedule
//
//  Created by Xcode on 20/12/2016.
//  Copyright © 2016 Team. All rights reserved.
//

#import "EditLessonViewController.h"

@interface EditLessonViewController ()<UITableViewDataSource,UITableViewDelegate>
@end

@implementation EditLessonViewController
- (IBAction)ChangeSettingsVisible:(UISwitch *)sender {
}
- (IBAction)ChangeTeacherVisible:(UISwitch *)sender {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.teachersTable.dataSource = self;
    self.teachersTable.delegate = self;
    self.model=[[NoteModel alloc] init];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    self.lessonHash=[def valueForKey:@"selHash"];
    NSString* note=[self.model loadNoteForLessonHash:self.lessonHash];
    self.noteTextView.text=note;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*) giveLesson:(NSString*)num{
    return num;
}
#pragma mark - TextView
-(void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"Можете здесь оставить вашу заметку"])
    {
        [textView setTextColor:[UIColor blackColor]];
        textView.text = @"";
    }
    [textView becomeFirstResponder];
}

-(void) textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""])
    {
        
        [textView setTextColor: [[UIColor blackColor]colorWithAlphaComponent:0.66f]];
        textView.text = @"Можете здесь оставить вашу заметку";
    }
    [textView resignFirstResponder];}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(nonnull NSString *)text{
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return false;
    }
    return true;
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    teacherCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"teacherCell"];
    
    if (cell == nil)
    {
        cell = [[teacherCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"teacherCell"];
    }
    
    cell.nameAndRoomLabel.text = @"index path";
    cell.typeAndNameLectionLabel.text = [self.data giveLesson:@"testeqqq"];
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Save:(id)sender {
    [self.model saveNote:self.noteTextView.text forLessonHash:self.lessonHash];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
