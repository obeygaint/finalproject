//
//  EditLessonViewController.h
//  Shedule
//
//  Created by Xcode on 20/12/2016.
//  Copyright © 2016 Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "teacherCellTableViewCell.h"
#import "WeekViewController.h"
#import "NoteModel.h"

@protocol  giveInfo <NSObject>

-(NSString*) giveLesson:(NSString*)num;

@end

@interface EditLessonViewController : UIViewController<UITextViewDelegate,UITextFieldDelegate,UITableViewDataSource,giveInfo>
@property (weak, nonatomic) IBOutlet UITableView *teachersTable;
@property (strong, nonatomic) IBOutlet UITextView *noteTextView;
@property (strong,nonatomic)id<giveInfo> data;
- (IBAction)Save:(id)sender;
@property (nonatomic,strong) NoteModel* model;
@property (nonatomic,strong) NSString* lessonHash;


@end
